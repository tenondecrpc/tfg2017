\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {xi}}{figure.caption.1}
\contentsline {chapter}{Abstract}{\es@scroman {xii}}{figure.caption.1}
\contentsline {chapter}{\'Indice de figuras y tablas}{\es@scroman {xiv}}{chapter*.3}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\'on}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Definici\'on del problema}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivos, hip\'otesis, justificaci\'on y delimitaci\'on del alcance del tratado.}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Objetivo General}{3}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Objetivo Espec\IeC {\'\i }ficos}{4}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Hip\'otesis}{4}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Justificaci\'on del Estudio.}{4}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}Alcance del Trabajo.}{4}{subsection.1.3.5}
\contentsline {section}{\numberline {1.4}Descripci\'on de los Contenidos por Cap\IeC {\'\i }tulo}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Tipo de Investigaci\'on}{5}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Investigaci\'on Tecnol\'ogica}{5}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Modalidad}{5}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Herramientas de recolecci\'on de datos}{5}{subsection.1.5.3}
\contentsline {chapter}{\numberline {2}Marco Te\'orico.}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Conceptos e ideas fundamentales.}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Stack}{6}{subsection.2.1.1}
\contentsline {subsubsection}{Utilizaci\'on de definiciones de software}{6}{section*.4}
\contentsline {subsubsection}{Uso de im\'agenes}{7}{section*.5}
\contentsline {subsubsection}{M\'etodos de instalaci\'on}{7}{section*.6}
\contentsline {subsection}{\numberline {2.1.2}Control de Versiones}{7}{subsection.2.1.2}
\contentsline {subsubsection}{Evoluci\'on de los sistemas de control de versiones}{8}{section*.7}
\contentsline {subsubsection}{Caracter\IeC {\'\i }sticas de un sistema de control de versiones}{8}{section*.8}
\contentsline {subsubsection}{Funcionalidad general de un Sistema de Control de Versiones}{9}{section*.9}
\contentsline {subsubsection}{Modelo Cliente Servidor}{9}{section*.10}
\contentsline {subsubsection}{Concurrencia}{9}{section*.11}
\contentsline {subsubsection}{Resoluci\'on de conflictos}{10}{section*.12}
\contentsline {subsubsection}{Gesti\'on de Cambios}{10}{section*.13}
\contentsline {subsubsection}{Historial de cambios}{10}{section*.14}
\contentsline {subsubsection}{Repositorios}{11}{section*.15}
\contentsline {subsubsection}{Ventajas de los Sistemas de Control de Versiones}{11}{section*.16}
\contentsline {subsubsection}{Clasificaci\'on de los Sistemas de Control de Versiones}{12}{section*.17}
\contentsline {subsubsection}{Sistemas de control de versiones centralizados}{12}{section*.18}
\contentsline {subsubsection}{Ventajas}{12}{section*.19}
\contentsline {subsubsection}{Desventajas}{12}{section*.20}
\contentsline {subsubsection}{Sistemas de control de versiones distribuidos}{12}{section*.21}
\contentsline {subsubsection}{Ventajas de los sistemas de control de versiones distribuidos}{13}{section*.22}
\contentsline {subsection}{\numberline {2.1.3}Git}{14}{subsection.2.1.3}
\contentsline {subsubsection}{Caracter\IeC {\'\i }sticas m\'as relevantes de GIT}{14}{section*.23}
\contentsline {subsubsection}{Comandos m\'as utilizados de GIT}{15}{section*.24}
\contentsline {subsubsection}{Estados de Git}{15}{section*.25}
\contentsline {subsubsection}{Componentes de Git}{16}{section*.26}
\contentsline {subsubsection}{Flujo de Trabajo con Git}{16}{section*.27}
\contentsline {subsection}{\numberline {2.1.4}Gitlab}{17}{subsection.2.1.4}
\contentsline {subsubsection}{Caracter\IeC {\'\i }sticas y Funcionalidades de Gitlab}{17}{section*.28}
\contentsline {subsection}{\numberline {2.1.5}Tarea}{17}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Gestor de Tareas}{18}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Beneficios de la gesti\'on de tareas}{18}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}Tablero Kanban}{19}{subsection.2.1.8}
\contentsline {subsection}{\numberline {2.1.9}Trello}{19}{subsection.2.1.9}
\contentsline {subsection}{\numberline {2.1.10}IDE}{21}{subsection.2.1.10}
\contentsline {section}{\numberline {2.2}Antecedentes}{22}{section.2.2}
\contentsline {chapter}{\numberline {3}Contexto del trabajo.}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Uso de los Stacks en la materia de Inform\'atica IV de la Facultad Polit\'ecnica UNE}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Stack m\'as utilizado por alumnos del Cuarto Semestre de la Facultad Polit\'ecnica UNE de la carrera An\'alisis de Sistemas}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}Capacitaci\'on para el uso de un stack de cuatro componentes}{25}{section.3.3}
\contentsline {section}{\numberline {3.4}Dificultades para la adopci\'on del stack}{26}{section.3.4}
\contentsline {section}{\numberline {3.5}Ventajas y desventajas para la evaluaci\'on por parte del docente utilizando el stack propuesto}{27}{section.3.5}
\contentsline {section}{\numberline {3.6}Diferencia de tiempo en la entrega del proyecto utilizando el stack propuesto y el stack tradicional}{27}{section.3.6}
\contentsline {chapter}{\numberline {4}M\'etodos.}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Tipo y nivel de la investigaci\'on.}{29}{section.4.1}
\contentsline {section}{\numberline {4.2}Poblaci\'on y muestra.}{29}{section.4.2}
\contentsline {section}{\numberline {4.3}Herramientas de recolecci\'on de datos.}{29}{section.4.3}
\contentsline {section}{\numberline {4.4}Resultados}{30}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}An\'alisis de los resultados}{30}{subsection.4.4.1}
\contentsline {subsubsection}{1- \IeC {\textquestiondown }Stack m\'as utilizado?}{30}{section*.29}
\contentsline {subsubsection}{2- \IeC {\textquestiondown }Cu\'al es la composici\'on de equipos en el desarrollo de software?}{32}{section*.32}
\contentsline {subsubsection}{3- \IeC {\textquestiondown }Forma de coordinaci\'on de actividades entre un equipo?}{33}{section*.35}
\contentsline {subsubsection}{4- \IeC {\textquestiondown }Forma de administraci\'on de los archivos del proyecto?}{34}{section*.38}
\contentsline {subsubsection}{5- \IeC {\textquestiondown }Forma de realizar copias diarias del proyecto?}{36}{section*.41}
\contentsline {subsubsection}{1- \IeC {\textquestiondown }Como fue el tiempo de aprendizaje del nuevo stack de cuatro componentes?}{37}{section*.44}
\contentsline {subsubsection}{2- \IeC {\textquestiondown }La complejidad del aprendizaje es?}{39}{section*.47}
\contentsline {subsubsection}{3- \IeC {\textquestiondown }La aplicaci\'on del stack ser\'a favorable al desarrollo de la asignatura de Inform\'atica IV?}{40}{section*.50}
\contentsline {subsubsection}{4- \IeC {\textquestiondown }Lo aprendido fue beneficioso para el logro de la materia?}{41}{section*.53}
\contentsline {subsubsection}{5- \IeC {\textquestiondown }Los conocimientos previos aportados por la carrera fueron \'utiles para la utilizaci\'on del stack de cuatro componentes?}{43}{section*.56}
\contentsline {chapter}{\numberline {5}Conclusi\'on}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Trabajos Futuros}{45}{section.5.1}
\contentsline {chapter}{Referencias bibliogr\'aficas}{46}{section.5.1}
